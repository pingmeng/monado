---
- mr.2058
- mr.2125
---

introduce VIT loader to load a given VIT system, implement the VIT interface
in slam tracker and remove the unused MatFrame class.
